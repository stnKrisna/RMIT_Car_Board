/******************************************************************************
** Student name:  Stanislaus Krisna
** Student number:   s3703579
** Course:        Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "carboard.h"

int main() {
  displayMainMenu();

  return(EXIT_SUCCESS);
}

void displayMainMenu() {
  int doLoop = 1;

  printf("Welcome to Car Board\n--------------------\n1. Play game\n2. Show student’s information\n3. Quit\n");

  do {
    /* Wait for user selection */
    char userSelection [MAIN_MENU_INPUT_BUFFER];
    printf("\nPlease enter your choice: ");
    fgets(userSelection, MAIN_MENU_INPUT_BUFFER, stdin);
    readRestOfLine();

    /* Convert user selection to int */

    switch ((int)userSelection[0]) {
    case PLAY_GAME_MENU:
      playGame();
      break;

    case SHOW_STUDENT_INFO_MENU:
      showStudentInformation();
      break;

    case EXIT_APP_MENU:
      return;

      break;

    default: break;
    }

    doLoop = TRUE;
    readRestOfLine();
  } while (doLoop);
}

void showStudentInformation() {
  printf("\n----------------------------------\nName: %s\n", STUDENT_NAME);
  printf("No: %s\n", STUDENT_ID);
  printf("Email: %s\n----------------------------------\n", STUDENT_EMAIL);
}
