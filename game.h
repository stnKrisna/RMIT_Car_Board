/******************************************************************************
** Student name:  Stanislaus Krisna
** Student number:  s3703579
** Course:      Advanced Programming Techniques - S1 2018
******************************************************************************/

#ifndef GAME_H
#define GAME_H

#include "helpers.h"
#include "board.h"
#include "player.h"

#define COMMAND_LOAD "load"
#define COMMAND_INIT "init"
#define COMMAND_FORWARD "forward"
#define COMMAND_FORWARD_SHORTCUT "f"
#define COMMAND_TURN_LEFT "turn_left"
#define COMMAND_TURN_LEFT_SHORTCUT "l"
#define COMMAND_TURN_RIGHT "turn_right"
#define COMMAND_TURN_RIGHT_SHORTCUT "r"
#define COMMAND_QUIT "quit"

#define DIRECTION_NORTH "north"
#define DIRECTION_EAST "east"
#define DIRECTION_SOUTH "south"
#define DIRECTION_WEST "west"

#define COMMAND_INPUT_BUFFER        20
#define COMMAND_ARG_BUFFER          10
#define COMMAND_ARG_DEILIMITER      " "
#define COMMAND_INIT_DEILIMITER     ","
#define PREPARE_STATE               0
#define LOAD_STATE                  1
#define PLAY_STATE                  2
#define DO_COMMAND_LOOP             99

#define NUMBER_OF_COMMANDS (sizeof (availableCommands) / sizeof (const char *))

/**
 * Main menu option 1 - play the game as per the specification.
 *
 * This function makes all the calls to board & player and handles interaction
 * with the user (reading input from the console, error checking, etc...).
 *
 * It should be possible to break this function down into smaller functions -
 * but this is up to you to decide and is entirely your own design. You can
 * place the additional functions in this header file if you want.
 *
 * Note that if you don't break this function up it could become pretty big...
 */
void playGame();

/* Game data structure */
struct GameData {
        Cell board[BOARD_HEIGHT][BOARD_WIDTH];
        Player *player;
};

/* Command line helper function */
int commandCount();
int execCommand(char **, int, struct GameData *);
char *getLine();
char **splitLine(char *, char *);

/* Define commands */
int cmd_load(char **args, int gameState, struct GameData *gameData);
int cmd_init(char **args, int gameState, struct GameData *gameData);
int cmd_fwd(char **args, int gameState, struct GameData *gameData);
int cmd_turnLeft(char **args, int gameState, struct GameData *gameData);
int cmd_turnRight(char **args, int gameState, struct GameData *gameData);
int cmd_quit(char **args, int gameState, struct GameData *gameData);

#endif
