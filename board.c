/******************************************************************************
** Student name:  Stanislaus Krisna
** Student number:  s3703579
** Course:      Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "board.h"

Cell BOARD_1[BOARD_HEIGHT][BOARD_WIDTH] = {
  { BLOCKED, EMPTY,   EMPTY,   EMPTY, EMPTY,   EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY   },
  { EMPTY,   BLOCKED, EMPTY,   EMPTY, EMPTY,   EMPTY,   EMPTY,   BLOCKED, EMPTY, EMPTY   },
  { EMPTY,   EMPTY,   BLOCKED, EMPTY, EMPTY,   EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY   },
  { EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY,   EMPTY,   EMPTY,   BLOCKED, EMPTY, EMPTY   },
  { EMPTY,   EMPTY,   EMPTY,   EMPTY, BLOCKED, EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY   },
  { EMPTY,   EMPTY,   BLOCKED, EMPTY, EMPTY,   BLOCKED, EMPTY,   BLOCKED, EMPTY, EMPTY   },
  { EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY,   EMPTY,   BLOCKED, EMPTY,   EMPTY, EMPTY   },
  { EMPTY,   EMPTY,   BLOCKED, EMPTY, EMPTY,   EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY   },
  { EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY,   EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY   },
  { EMPTY,   EMPTY,   BLOCKED, EMPTY, EMPTY,   EMPTY,   EMPTY,   EMPTY,   EMPTY, BLOCKED }
};

Cell BOARD_2[BOARD_HEIGHT][BOARD_WIDTH] = {
  { BLOCKED, BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY,   BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY,   BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY,   EMPTY,   EMPTY,   EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY,   EMPTY,   EMPTY,   EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY,   BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY,   EMPTY,   EMPTY,   EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY,   BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY,   BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY,   BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY }
};

/* Empty board template */
Cell BOARD_EMPTY[BOARD_HEIGHT][BOARD_WIDTH] = {
  { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY },
  { EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY }
};

Boolean playerInBoard(struct position *p) {
  int x = p->x, y = p->y;

  if (x < 0 || y < 0) {
    return(FALSE);
  }

  if (x >= BOARD_WIDTH || y >= BOARD_HEIGHT) {
    return(FALSE);
  }

  return(TRUE);
}

void createBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH],
                 Cell boardToLoad[BOARD_HEIGHT][BOARD_WIDTH]) {
  int i, j;

  for (i = 0; i < BOARD_HEIGHT; i++) {
    for (j = 0; j < BOARD_WIDTH; j++) {
      board[i][j] = boardToLoad[i][j];
    }
  }
}

void initialiseBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH]) {
  createBoard(board, BOARD_EMPTY);
}

void loadBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH],
               Cell boardToLoad[BOARD_HEIGHT][BOARD_WIDTH]) {
  createBoard(board, boardToLoad);
}

Boolean isEmptyCell(Cell board[BOARD_HEIGHT][BOARD_WIDTH], Position position) {
  int x, y;

  x = position.x;
  y = position.y;

  return(board[y][x] == EMPTY);
}

Boolean placePlayer(Cell board[BOARD_HEIGHT][BOARD_WIDTH], Position position) {
  return(isEmptyCell(board, position) && playerInBoard(&position));
}

PlayerMove movePlayerForward(Cell board[BOARD_HEIGHT][BOARD_WIDTH],
                             Player *player) {
  Position newPos = getNextForwardPosition(player);

  if (!playerInBoard(&newPos)) {
    printf("The car is at the edge of the board and cannot move further in that direction.\n");
    return(OUTSIDE_BOUNDS);
  }

  if (!isEmptyCell(board, newPos)) {
    printf("Error: cannot move forward because the road is blocked.\n");
    return(CELL_BLOCKED);
  }

  updatePosition(player, newPos);

  return(PLAYER_MOVED);
}

void displayBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH], Player *player) {
  int i, j;

  printf("\n");
  for (i = 0; i < BOARD_HEIGHT + ROW_OFFSET; i++) {
    for (j = 0; j < BOARD_WIDTH + COLUMN_OFFSET; j++) {
      if (i == COLUMN_HEADER) {
        if (i == j) {
          printf("|   ");
          continue;
        }
        printf("| %d ", j - COLUMN_OFFSET);
      }else{
        if (j == ROW_HEADER) {
          printf("| %d ", i - ROW_OFFSET);
        }else{
          switch (board[i - ROW_OFFSET][j - COLUMN_OFFSET]) {
          case EMPTY:
            if (
              player != NULL &&
              player->position.x == j - COLUMN_OFFSET &&
              player->position.y == i - ROW_OFFSET) {
              displayDirection(player->direction);
            }else{
              printf("| %s ", EMPTY_OUTPUT);
            }
            break;

          default:
            printf("| %s ", BLOCKED_OUTPUT);
          }
        }
      }
    }
    printf("|\n");
  }

  printf("\n");
}
