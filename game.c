/******************************************************************************
** Student name:  Stanislaus Krisna
** Student number:   s3703579
** Course:        Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "game.h"

/* List of available commands */
const char *availableCommands[] = {
  COMMAND_LOAD,
  COMMAND_INIT,
  COMMAND_FORWARD,
  COMMAND_FORWARD_SHORTCUT,
  COMMAND_TURN_LEFT,
  COMMAND_TURN_LEFT_SHORTCUT,
  COMMAND_TURN_RIGHT,
  COMMAND_TURN_RIGHT_SHORTCUT,
  COMMAND_QUIT
};

int (*defaultCommandFunction[])(char **, int, struct GameData *) = {
  &cmd_load,
  &cmd_init,
  &cmd_fwd,
  &cmd_fwd,
  &cmd_turnLeft,
  &cmd_turnLeft,
  &cmd_turnRight,
  &cmd_turnRight,
  &cmd_quit
};

int invalidCommand() {
  printf(ERROR_INVALID_INPUT);
  return(FALSE);
}

int cmd_load(char **args, int gameState, struct GameData *gameData) {
  Cell(*boardToLoad)[BOARD_HEIGHT][BOARD_WIDTH];

  if (gameState == PLAY_STATE) {
    return(invalidCommand());
  }

  /* Check if the argument passed is a valid int */
  if (!isValidInteger(args[1])) {
    printf(ERROR_EXPECT_INTEGER, 1);
    return(DO_COMMAND_LOOP);
  }

  switch (atoi(args[1])) {
  case 1:
    boardToLoad = &BOARD_1;
    break;

  case 2:
    boardToLoad = &BOARD_2;
    break;

  default:
    printf(ERROR_BOARD_ID);
    return(FALSE);
  }

  createBoard(gameData->board, *boardToLoad);
  displayBoard(gameData->board, NULL);

  return(TRUE);
}

int correctInitDirection(char *dir) {
  if (strcmp(dir, DIRECTION_NORTH) == 0 ||
      strcmp(dir, DIRECTION_EAST) == 0 ||
      strcmp(dir, DIRECTION_SOUTH) == 0 ||
      strcmp(dir, DIRECTION_WEST) == 0) {
    return(TRUE);
  }

  return(FALSE);
}

int cmd_init(char **args, int gameState, struct GameData *gameData) {
  char **           params;
  struct position * p;
  int       i;
  Direction direction = NORTH;

  gameData->player = (struct player *)malloc(sizeof(struct player));
  p = (struct position *)malloc(sizeof(struct position));

  /* Check for proper game state */
  if (gameState != LOAD_STATE) {
    return(invalidCommand());
  }

  /* Check if arguments is given */
  if (args[1] == NULL) {
    return(invalidCommand());
  }

  /* Check for argument count */
  params = splitLine(args[1], COMMAND_INIT_DEILIMITER);
  for (i = 0; i < 3; i++) {
    if (params[i] == NULL) {
      return(invalidCommand());
    }
  }

  /* Check argument 1 & 2 */
  if (!isValidInteger(params[0])) {
    printf(ERROR_EXPECT_INTEGER, 1);
    return(DO_COMMAND_LOOP);
  }
  if (!isValidInteger(params[1])) {
    printf(ERROR_EXPECT_INTEGER, 2);
    return(DO_COMMAND_LOOP);
  }

  /* Check direction */
  if (strcmp(params[2], DIRECTION_NORTH) == 0) {
    direction = NORTH;
  }else if (strcmp(params[2], DIRECTION_EAST) == 0) {
    direction = EAST;
  }else if (strcmp(params[2], DIRECTION_SOUTH) == 0) {
    direction = SOUTH;
  }else if (strcmp(params[2], DIRECTION_WEST) == 0) {
    direction = WEST;
  }else{
    printf(ERROR_INVALID_DIRECTION);
    return(DO_COMMAND_LOOP);
  }

  /* Set position */
  p->x  = atoi(params[0]);
  p->y  = atoi(params[1]);

  /* Check if the third argument is one of the init direction */
  if (!correctInitDirection(params[2])) {
    return(invalidCommand());
  }

  /* Check for empty cell */
  if (!placePlayer(gameData->board, *p)) {
    printf(ERROR_PLACE_OUTSIDE_BOUNDS);
    return(DO_COMMAND_LOOP);
  }

  initialisePlayer(gameData->player, p, direction);

  return(TRUE);
}

int cmd_fwd(char **args, int gameState, struct GameData *gameData) {
  PlayerMove hasMove = movePlayerForward(gameData->board, gameData->player);

  if (gameState != PLAY_STATE) {
    return(invalidCommand());
  }

  if (hasMove == OUTSIDE_BOUNDS) {
    printf(ERROR_OUTSIDE_BOUNDS);
    return(DO_COMMAND_LOOP);
  }

  if (hasMove == CELL_BLOCKED) {
    printf(ERROR_CELL_BLOCKED);
    return(DO_COMMAND_LOOP);
  }

  return(TRUE);
}

int cmd_turnLeft(char **args, int gameState, struct GameData *gameData) {
  if (gameState != PLAY_STATE) {
    return(invalidCommand());
  }

  turnDirection(gameData->player, TURN_LEFT);
  return(TRUE);
}

int cmd_turnRight(char **args, int gameState, struct GameData *gameData) {
  if (gameState != PLAY_STATE) {
    return(invalidCommand());
  }

  turnDirection(gameData->player, TURN_RIGHT);
  return(TRUE);
}

int cmd_quit(char **args, int gameState, struct GameData *gameData) {
  printf("Total player moves: %u\n", gameData->player->moves);
  return(FALSE);
}

void printGameInstruction() {
  printf("You can use the following commands to play the game:\n");
  printf("load <g>\n");
  printf("   g: number of the game board to load\n");
  printf("init <x>,<y>,<direction>\n");
  printf("   x: horizontal position of the car on the board (between 0 & 9)\n");
  printf("   y: vertical position of the car on the board (between 0 & 9)\n");
  printf("   direction: direction of the car’s movement (north, east, south, west)\n");
  printf("forward (or f)\n");
  printf("turn_left (or l)\n");
  printf("turn_right (or r)\n");
  printf("quit\n");
  return;
}

int commandCount() {
  return(sizeof(availableCommands) / sizeof(char *));
}

int execCommand(char **args, int gameState, struct GameData *gameData) {
  int i;

  if (args[0] == NULL) {
    printf("Invalid Input.\n");
    return(FALSE);
  }

  for (i = 0; i < commandCount(); i++) {
    if (strcmp(args[0], availableCommands[i]) == 0) {
      return((*defaultCommandFunction[i])(args, gameState, gameData));
    }
  }

  return(invalidCommand());
}

char *getLine() {
  int   pos     = 0;
  char *buffer  = malloc(sizeof(char) * COMMAND_INPUT_BUFFER);
  int   c;

  /* Check for input buffer */
  if (!buffer) {
    return(0);
  }

  /* Tokenize the current line */
  while (TRUE) {
    c = getchar();

    /* return if the current char is the end of the line */
    if (c == EOF || c == '\n') {
      buffer[pos] = '\0';
      return(buffer);
    }
    /* If not the end of the line, add current character to the buffer */
    else{
      buffer[pos] = c;
    }

    pos++;

    /* return line when buffer has been exceeded */
    if (pos >= COMMAND_INPUT_BUFFER) {
      return(buffer);
    }
  }
}

char **splitLine(char *line, char *delim) {
  int   bufferSize  = COMMAND_ARG_BUFFER;
  int   pos         = 0;

  char **   args = malloc(bufferSize * sizeof(char *));
  char *    arg;

  /* Return empty array if allocation fails */
  if (!args) {
    args = malloc(bufferSize * sizeof(char *));
    return(args);
  }

  arg = strtok(line, delim);
  while (arg != NULL) {
    args[pos] = arg;
    pos++;

    /* Return array when buffer has been exceeded  */
    if (pos >= bufferSize) {
      return(args);
    }

    arg = strtok(NULL, delim);
  }

  args[pos] = NULL;
  return(args);
}

int processCommand(int gameState, struct GameData *gameData) {
  char *    line;
  char **   args;
  int       status = FALSE;

  do {
    printf("> ");
    line    = getLine();
    args    = splitLine(line, COMMAND_ARG_DEILIMITER);

    status = execCommand(args, gameState, gameData);
    if (status == FALSE) {
      return(FALSE);
    }

    if (gameState == LOAD_STATE &&
        strcmp(args[0], COMMAND_INIT) != '\0') {
      status = DO_COMMAND_LOOP;
    }

    free(line);
    free(args);
  } while (status == DO_COMMAND_LOOP);

  return(TRUE);
}

void playGame() {
  struct GameData *gameData = (struct GameData *)malloc(sizeof(struct GameData));

  /* Prepare game state */
  printGameInstruction();
  initialiseBoard(gameData->board);
  displayBoard(gameData->board, NULL);
  fflush(stdin);

  /* Prompt to load board */
  if (!processCommand(PREPARE_STATE, gameData)) {
    return;
  }

  /* Prompt to change board */
  if (!processCommand(LOAD_STATE, gameData)) {
    return;
  }

  /* Start game loop */
  while (TRUE) {
    displayBoard(gameData->board, gameData->player);
    if (!processCommand(PLAY_STATE, gameData)) {
      return;
    }
  }
}
