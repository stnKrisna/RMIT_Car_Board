/******************************************************************************
** Student name:  Stanislaus Krisna
** Student number:  s3703579
** Course:      Advanced Programming Techniques - S1 2018
******************************************************************************/

#ifndef HELPERS_H
#define HELPERS_H

/* Error messages */
#ifndef ERROR_OUTSIDE_BOUNDS
#define ERROR_OUTSIDE_BOUNDS "The car is at the edge of the board and cannot move further in that direction.\n"
#endif

#ifndef ERROR_CELL_BLOCKED
#define ERROR_CELL_BLOCKED "Cannot move forward because the road is blocked.\n"
#endif

#ifndef ERROR_PLACE_OUTSIDE_BOUNDS
#define ERROR_PLACE_OUTSIDE_BOUNDS "Cannot place player outside of the board or on none free cells.\n"
#endif

#ifndef ERROR_INVALID_DIRECTION
#define ERROR_INVALID_DIRECTION "Error: Invalid direction.\n"
#endif

#ifndef ERROR_EXPECT_INTEGER
#define ERROR_EXPECT_INTEGER "Parameter %d must be an integer\n"
#endif

#ifndef ERROR_BOARD_ID
#define ERROR_BOARD_ID "Parameter 1 must be 1 or 2\n"
#endif

#ifndef ERROR_INVALID_INPUT
#define ERROR_INVALID_INPUT "Invalid Input.\n"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

typedef enum boolean
{
        FALSE = 0,
        TRUE
} Boolean;

#define NEW_LINE_SPACE 1
#define NULL_SPACE 1

/**
 * This is used to compensate for the extra character spaces taken up by
 * the '\n' and '\0' when input is read through fgets().
 **/
#define EXTRA_SPACES (NEW_LINE_SPACE + NULL_SPACE)

#define EMPTY_STRING ""

/**
 * Call this function whenever you detect buffer overflow but only call this
 * function when this has happened.
 **/
void readRestOfLine();

int strlen_noNewLine();

Boolean isValidInteger(char *);

#endif
