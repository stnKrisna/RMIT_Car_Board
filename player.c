/******************************************************************************
** Student name:    Stanislaus Krisna
** Student number:  s3703579
** Course:          Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "player.h"

void initialisePlayer(Player *player, Position *position, Direction direction) {
  player->position  = *position;
  player->moves     = 0;
  player->direction = direction;
}

void turnDirection(Player *player, TurnDirection turnDirection) {
  Direction d = player->direction;

  switch (turnDirection) {
  case TURN_RIGHT:
    switch (d) {
    case NORTH: player->direction = EAST; return;

    case EAST: player->direction = SOUTH; return;

    case SOUTH: player->direction = WEST; return;

    case WEST: player->direction = NORTH; return;
    }
    return;

  case TURN_LEFT:
    switch (d) {
    case NORTH: player->direction = WEST; return;

    case EAST: player->direction = NORTH; return;

    case SOUTH: player->direction = EAST; return;

    case WEST: player->direction = SOUTH; return;
    }
    return;
  }
}

Position getNextForwardPosition(const Player *player) {
  Position position = player->position;

  switch (player->direction) {
  case NORTH:
    position.y--;
    break;

  case EAST:
    position.x++;
    break;

  case SOUTH:
    position.y++;
    break;

  case WEST:
    position.x--;
    break;
  }

  return(position);
}

void updatePosition(Player *player, Position position) {
  player->position = position;
  ++player->moves;
}

void displayDirection(Direction direction) {
  switch (direction) {
  case NORTH: printf("| %s ", DIRECTION_ARROW_OUTPUT_NORTH); return;

  case EAST: printf("| %s ", DIRECTION_ARROW_OUTPUT_EAST); return;

  case SOUTH: printf("| %s ", DIRECTION_ARROW_OUTPUT_SOUTH); return;

  case WEST: printf("| %s ", DIRECTION_ARROW_OUTPUT_WEST); return;
  }
}
