/******************************************************************************
** Student name:    Stanislaus Krisna
** Student number:  s3703579
** Course:          Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "helpers.h"

void readRestOfLine() {
  int ch;

  while (ch = getc(stdin), ch != EOF && ch != '\n') {
  }     /* Gobble each character. */

  /* Reset the error status of the stream. */
  clearerr(stdin);
}

Boolean isValidChar(char c) {
  /* Return true if the given char is greater than ascii 33 */
  return(c >= '!');
}

Boolean isValidInteger(char *inputChar) {
  int i = 0;

  while (isValidChar(inputChar[i])) {
    if (!(inputChar[i] >= '0' && inputChar[i] <= '9')) {
      return(FALSE);
    }
    i++;
  }

  return(TRUE);
}
