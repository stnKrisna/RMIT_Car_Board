/******************************************************************************
** Student name:  Stanislaus Krisna
** Student number:  s3703579
** Course:      Advanced Programming Techniques - S1 2018
******************************************************************************/

#ifndef CARBOARD_H
#define CARBOARD_H

#include "helpers.h"
#include "game.h"

#define STUDENT_NAME "Stanislaus Krisna"
#define STUDENT_ID "s3703579"
#define STUDENT_EMAIL "s3703579@student.rmit.edu.au"

#define PLAY_GAME_MENU              49
#define SHOW_STUDENT_INFO_MENU      50
#define EXIT_APP_MENU               51
#define MAIN_MENU_INPUT_BUFFER      2

void displayMainMenu();
/**
 * Main menu option 2 - show your student information as per the specification.
 *
 * You should change the defines above related to student information and use
 * them when printing.
 */
void showStudentInformation();

#endif
